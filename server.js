const chalk = require('chalk');
const express = require('express')
const app = express()

app.get('/', function (req, res) {
  res.send('Hello World')
})

app.listen(3000, function(err){
  if(err){
    console.log(chalk.red('Error: ' + err ));
    return err;
  }
  console.log( chalk.green('Server is up and running on port 3000'))
} )
